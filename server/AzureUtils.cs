using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

/*
Based on example from https://docs.microsoft.com/en-us/azure/storage/blobs/storage-dotnet-shared-access-signature-part-2
 */
namespace azure
{
    public class AzureUtils
    {
        public const SharedAccessBlobPermissions WritePermission=SharedAccessBlobPermissions.Write;
        public const SharedAccessBlobPermissions ReadPermission=SharedAccessBlobPermissions.Read;
        public const SharedAccessBlobPermissions ListPermission=SharedAccessBlobPermissions.List;

        private static string _GetContainerSasToken(CloudBlobContainer container, SharedAccessBlobPermissions permissions)
        {
            //Set the expiry time and permissions for the container.
            //In this case no start time is specified, so the shared access signature becomes valid immediately.
            SharedAccessBlobPolicy sasConstraints = new SharedAccessBlobPolicy();
            sasConstraints.SharedAccessExpiryTime = DateTimeOffset.UtcNow.AddMinutes(10); //10 minutes last
            //sasConstraints.Permissions = SharedAccessBlobPermissions.List | SharedAccessBlobPermissions.Write;
            sasConstraints.Permissions = permissions;

            //Generate the shared access signature on the container, setting the constraints directly on the signature.
            return container.GetSharedAccessSignature(sasConstraints);
        }

        public static string GetSasToken(string accountName, string accountKey, string containerName, SharedAccessBlobPermissions permissions){
                       
            //Parse the connection string and return a reference to the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName="+accountName+";AccountKey="+accountKey);

            //Create the blob client object.
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            //Get a reference to a container to use for the sample code, and create it if it does not exist.
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);
            container.CreateIfNotExistsAsync();

            return _GetContainerSasToken(container, permissions);

        }

    }

}
