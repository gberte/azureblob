﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace azure.Controllers
{
    [Route("/api/getazurekey")]
    [EnableCors("Allowall")]
    [Produces("application/json")]
    public class DefaultController : Controller
    {   
        [HttpGet]
        public string GetAccesToken()
        {
            string accountName= "[AZURE_ACCOUNT_NAME]";
            string accountKey= "[AZURE_ACCOUNT_KEY]";
            string containerName="hecontainer";
            return  AzureUtils.GetSasToken(accountName,accountKey, containerName,AzureUtils.WritePermission);
        }
    }
}
