//With account
//var accountKey ="MGbbixA3buO0lpEdNs3dKCy95GUuungtX7zM/q9omGs+z2J+idfFw/nApYM6b+vmPBs97yLGn3sqzyJgA2pwQg==";
//var account = "gusberte";
//var blobService = AzureStorage.createBlobService(account, accountKey);
/*blobService.createContainerIfNotExists('hescontainer', function(error, result) {
    if (error) {
        // Create container error
        console.log(error)
    } else {
        // Create container successfully
    }
});*/

//With sas
var blobService,sasKey,
accountName= "gusberte",
blobUri = "https://"+accountName+".blob.core.windows.net", 
keyEndPoint="http://localhost:5000/api/getazurekey",
containerName="hecontainer",
speedSummary;

//Getting the saskey
var xhttp = new XMLHttpRequest(); 
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
  	sasKey = JSON.parse(this.responseText);
  	console.log(sasKey);
	blobService = AzureStorage.createBlobServiceWithSas(blobUri, sasKey);
  }else{

  }
};
xhttp.open("GET", keyEndPoint, true);
xhttp.send();

document.getElementById("fileinput").addEventListener("change", function(){
	var file = document.getElementById('fileinput').files[0];
	var customBlockSize = file.size > 1024 * 1024 * 32 ? 1024 * 1024 * 4 : 1024 * 512;
	blobService.singleBlobPutThresholdInBytes = customBlockSize;

	console.log(file.name);

	var finishedOrError = false;
	speedSummary = blobService.createBlockBlobFromBrowserFile(containerName, file.name, file, {blockSize : customBlockSize}, function(error, result, response) {
	    finishedOrError = true;
	    if (error) {
	        // Upload blob failed
	        displayStatus("An error has ocurred:"+ error);
	    } else {
	        // Upload successfully
	        var url = blobUri+"/"+result.container+"/"+result.name;
	        displayStatus("It is up :-). You can download the file from: <a href='"+url+"'>"+url+"</a>");
	    }
	});

	//Refreshing the progress bar
    var interval= setInterval(function() {
	   	var process = speedSummary.getCompletePercent();
        if (finishedOrError){
        	clearInterval(interval);
        }else{
        	displayStatus("Uploading:"+ process);
        } 
        
    },10);
});

function displayStatus(text){
	var file = document.getElementById('status');
	file.innerHTML=text;

}
